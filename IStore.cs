﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Refit;

namespace finalassignment
{
    public interface IStore
    {


        [Get("/v2/store/inventory")]
        Task<HttpResponseMessage> GetPetInventoryByStatus();

        [Get("/store/order/{orderId}")]
         Task<Refit.ApiResponse<Order>> GetPurchaseOrderById( int orderId  );

         [Post("/store/order")]
         Task<Refit.ApiResponse<APIResponse>> PostAnOrder([Body] Order order);

         [Delete("/store/order/{orderId}")]
         Task<HttpResponseMessage> DeleteAnOrder(Int64 orderId);

    }
}
