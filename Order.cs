﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace finalassignment
{
     public class Order
    {
        public Int64 Id { get; set; }
        public Int64 PetId { get; set; }
        public int Quantity { get; set; }
        public DateTime ShipDate { get; set; }
        public Boolean Complete { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public  OrderStatusEnum Status { get; set; }
         

    }
}
