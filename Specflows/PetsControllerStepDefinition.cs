﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Refit;
using TechTalk.SpecFlow;

namespace finalassignment
{
    [Binding]
    public sealed class PetsControllerStepDefinition
    {
   
        private readonly IPet _refitClient;
        private Refit.ApiResponse<Pet> addPetResponse;
        private Refit.ApiResponse<Pet> getPetResponse;
        private HttpResponseMessage deletePetResponse;

        public PetsControllerStepDefinition(ScenarioContext testcontext)
        {
            _refitClient = RestService.For<IPet>("https://petstore.swagger.io");
        }

        [When(@"I add a pet (.*), (.*), (.*), (.*), (.*), (.*), (.*)")]
        public async Task WhenIAddAPet(Int64 petId, int categoryId, string categoryName, string petName,
            Int64 tagId,string tagName, string petStatus )
        {
           var  inputmodel = new Pet()
            {
                Id = petId,
                PetCategory = new Category { Id = categoryId, Name = categoryName },
                Name  = petName,
                Tags =  new List<Tag>
                   { 
                       new Tag
                       {
                           Id=tagId,
                           Name=tagName
                       }
                   },
               //PhotoUrls = photoUrls,
               PetStatus = Enum.Parse<PetStatusEnum>(petStatus)
            };
            addPetResponse = await _refitClient.AddPet(inputmodel);
          await addPetResponse.EnsureSuccessStatusCodeAsync();
        }


        [Then(@"the pet is added by petId (.*)")]
        public async Task ThenThePetIsAddedByPetId(Int64 petId)
        {
            getPetResponse = await _refitClient.GetPetById(petId);
          await getPetResponse.EnsureSuccessStatusCodeAsync();
        }

       
        [When(@"I get a pet by Id (.*)")]
        public async Task WhenIGetAPetById(Int64 petId )
        {
            getPetResponse = await _refitClient.GetPetById(petId);
          
        }

        [Then(@"the pet exists")]
        public async Task ThenThePetExists()
        {

            await getPetResponse.EnsureSuccessStatusCodeAsync();
        }

        [When(@"I delete a pet by Id (.*)")]
        public async Task WhenIDeleteAPetById(Int64 petId)
        {
           deletePetResponse = await _refitClient.DeletePetById(petId);
        }

        [Then(@"the pet is removed")]
        public async Task ThenThePetIsRemoved()
        {
             deletePetResponse.EnsureSuccessStatusCode();
        }

        [When(@"I get a pet by status (.*)")]
        public async Task WhenIGetAPetByStatus(string status)
        {
            var test = await _refitClient.GetPetByStatus(status);
            var totalCount = test.Content?.Select(t => t.PetStatus == Enum.Parse<PetStatusEnum>(status)).Count();
            totalCount.Should().BeGreaterThan(0);
        }


        [When(@"I update a pet")]
        public void WhenIUpdateAPet()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the pet is updated")]
        public void ThenThePetIsUpdated()
        {
            ScenarioContext.Current.Pending();
        }



    }
}
