﻿Feature: OrderControllerTests
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

	
Scenario: Post an Order
	When I add an order <OrderId>, <PetId>, <OrderStatus>
	Examples:
	| OrderId | PetId | OrderStatus |
	| 5      | 202   | Placed      |
	| 7      | 203   | Approved    |
	| 9      | 204   | Delivered   |  

	@GetOrder
	Scenario Outline: Get an Order
	When I get an order by orderId <OrderId>
	Examples:
	| OrderId | 
	| 3     |
	| 4      |
	| 5      |  

	@DeleteOrder
	Scenario Outline:Delete Order
	When I delete an order by orderId <OrderId>
	Examples:
	| OrderId | 
	| 3      |
	| 4      |
	| 5      |  
