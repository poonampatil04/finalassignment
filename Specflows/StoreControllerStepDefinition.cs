﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Refit;
using TechTalk.SpecFlow;

namespace finalassignment
{
    [Binding]
    public sealed class StoreControllerStepDefinition
    {

        private readonly IStore _refitClient;
        private Refit.ApiResponse<Order> getOrdeResponse;
        private HttpResponseMessage deletePetResponse;

        public StoreControllerStepDefinition(ScenarioContext injectedContext)
        {
            _refitClient = RestService.For<IStore>("https://petstore.swagger.io");
        }

        [BeforeScenario("GetOrder","DeleteOrder")]
        public async Task ThisHookRunBeforeScenariosWithTestTag1()
        {
            await AddMultipleOrders();
        }

        
        private async Task AddMultipleOrders()
        {

            int[] orderIdArray = { 3, 4, 5 };
            foreach (int orderId in orderIdArray)
            {
                var testOrderInput = new Order
                {
                    Id = orderId,
                };
                var output = await _refitClient.PostAnOrder(testOrderInput);

            }
           
        }

        [When(@"I add an order (.*), (.*), (.*)")]
        public async Task WhenIAddAnOrder(Int64 orderId, Int64 petId, string orderStatus)
        {

            var testOrderInput = new Order
            {
                Id = orderId,
                Complete = true,
                PetId = petId,
                Quantity = 5,
                ShipDate = DateTime.Today,
                Status = Enum.Parse<OrderStatusEnum>(orderStatus)

            };
            var output =await _refitClient.PostAnOrder(testOrderInput);
        }

        [When(@"I get an order by orderId (.*)")]
        public async Task WhenIGetAnOrderByOrderId(int orderId)
        {
            getOrdeResponse =await _refitClient.GetPurchaseOrderById(orderId);
            await getOrdeResponse.EnsureSuccessStatusCodeAsync();
            getOrdeResponse.Content.Id.Should().Be(orderId);
        }

        [When(@"I delete an order by orderId (.*)")]
        public async Task WhenIDeleteAnOrderByOrderId(Int64 orderId)
        {
            HttpResponseMessage deleteOrderResponse = await _refitClient.DeleteAnOrder(orderId);
             deleteOrderResponse.EnsureSuccessStatusCode();
          
        }



    }
}
