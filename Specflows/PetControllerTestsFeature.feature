﻿Feature: PetControllerTestsFeature
	
Scenario Outline: 01 Add a pet 
	When I add a pet <Id>, <CategoryId>, <CategoryName>, <PetName>, <TagId>, <TagName>, <PetStatus>
	#Then the pet is added by petId <Id>
	#Examples: 
	#| Id  | CategoryId | CategoryName    | PetName    | PhotoUrls                         | TagId | TagName    | PetStatus |
	#| 207 | 2010       | TestCategory123 | PetRockie  | Test                              | 20100 | TestTag123 | Available |
	#| 202 | 2020       | TestCategory456 | PetBazukoo | http://example.com/images/rex.png | 20200 | TestTag456 | Available |
	#| 203 | 2030       | TestCategory234 | PetBali    | http://example.com/images/rex.png | 20300 | TestTag234 | Sold      |
	#| 204 | 2040       | TestCategory321 | PetThunder | http://example.com/images/rex.png | 20400 | TestTag321 | Pending   |
	Examples: 
	| Id  | CategoryId | CategoryName    | PetName    | TagId | TagName    | PetStatus |
	| 207 | 2010       | TestCategory123 | PetRockie  | 20100 | TestTag123 | Available |
	| 202 | 2020       | TestCategory456 | PetBazukoo | 20200 | TestTag456 | Available |
	| 203 | 2030       | TestCategory234 | PetBali    | 20300 | TestTag234 | Sold      |
	| 204 | 2040       | TestCategory321 | PetThunder | 20400 | TestTag321 | Pending   |

Scenario Outline: 02 Get a pet by pet id 
	When I get a pet by Id <PetId>
	Then the pet exists
	Examples:
	| PetId |
	| 505   |
	| 202   |
	| 203   |
	| 204   |

	Scenario Outline: 03 update an existing pet 
	When I update a pet  <Id>, <Quantity>, <OrderStatus>
	Then the pet is updated by petId <Id>
	Examples: 
	| Id  | CategoryId | CategoryName    | PetName    | PhotoUrls                         | TagId | TagName    | PetStatus |
	| 207 | 2010       | TestCategory123 | PetRockie  | http://example.com/images/rex.png | 20100 | TestTag123 |Available |
	| 202 | 2020       | TestCategory456 | PetBazukoo | http://example.com/images/rex.png | 20200 | TestTag456 | Available |
	| 203 | 2030       | TestCategory234 | PetBali    | http://example.com/images/rex.png | 20300 | TestTag234 | Sold      |
	| 204 | 2040       | TestCategory321 | PetThunder | http://example.com/images/rex.png | 20400 | TestTag321 |Pending   |  
	
Scenario Outline: 04 Delete a pet by pet id
	When I delete a pet by Id <PetId>
	Then the pet is removed 
	Examples:
	| PetId |
	| 505   |
	| 202   |
	| 203   |
	| 204   |

	Scenario Outline: 04 get pets by status
	When I get a pet by status <Status>
	
	Examples:
	| Status    |
	| available |
	| pending   |
	| sold      |

	
