﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace finalassignment
{
   public class Pet
    {
        public Int64 Id { get; set; }
        public Category PetCategory{ get; set; }
        public string Name { get; set; }
       // public string PhotoUrls { get; set; }
        public IEnumerable<Tag> Tags { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public PetStatusEnum PetStatus { get; set; }
    }
}
