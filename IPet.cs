﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Refit;

namespace finalassignment
{
    public interface IPet
    {
        [Get("/v2/pet/{petId}")]
        Task<Refit.ApiResponse<Pet>> GetPetById(Int64 petId);

        [Post("/v2/pet")]
        Task<Refit.ApiResponse<Pet>> AddPet([Body] Pet petInfo);

        [Get("/v2/pet/findByStatus?status={status}")]
        Task<Refit.ApiResponse<IEnumerable<Pet>>> GetPetByStatus(string status);

        //[Post("/v2/pet/{petId}/uploadImage")]
        //Task<HttpResponseMessage> AddImageForPet(int petId);

        //[Put("/v2/pet")]
        //Task<HttpResponseMessage> UpdatePetInfo([Body] Pet petInfo);


        [Delete("/v2/pet/{petId}")]
        Task<HttpResponseMessage> DeletePetById(Int64 petId);


    }
}
